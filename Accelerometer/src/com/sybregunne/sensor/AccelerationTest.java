package com.sybregunne.sensor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Button;


public class AccelerationTest extends Activity implements SensorEventListener, OnCheckedChangeListener,
	Gyroscope.OnOrientationChangeListener, OnClickListener {

	private float mLastX, mLastY, mLastZ, dX, dY, dZ, dG;
	private boolean mInitialized; 
	private SensorManager mSensorManager; 
	private Sensor mAccelerometer;
	private float NOISE = (float) 2.0;
	private CheckBox k,g;
	//private TextView sens;
	private static  boolean wG=true,hardBrake=false;
	private Gyroscope gyro;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_acceleration_test);
		
		mInitialized = false;
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		k = (CheckBox) findViewById(R.id.keep_screen_on);
		g = (CheckBox) findViewById(R.id.include_gravity);
		k.setOnCheckedChangeListener(this);
		g.setOnCheckedChangeListener(this);
		k.setChecked(true);
		g.setChecked(wG);
		Button hbclear = (Button) findViewById(R.id.clear);
		hbclear.setOnClickListener(this);
		//sens = (TextView) findViewById(R.id.sens);
		Button plus = (Button) findViewById(R.id.sens_add);
		Button minus = (Button) findViewById(R.id.sens_sub);
		plus.setOnClickListener(this);
		minus.setOnClickListener(this);
		gyro = new Gyroscope(this);
		gyro.setOnOrientationChangedListener(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.acceleration_test, menu);
		return true;
	}

	protected void onResume() {
		super.onResume();
		gyro.register();
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
		gyro.deRegister();
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// can be safely ignored for this demo
	}
	private float getG(float x, float y, float z, boolean wGravity) {
		float result = 0.0f;
		result = (float) //(
				Math.sqrt(x*x+y*y+z*z);//-SensorManager.GRAVITY_EARTH)/SensorManager.GRAVITY_EARTH;
		//result = Math.round(result*10000)/10000;
		if (!wGravity) result = result-SensorManager.GRAVITY_EARTH;
		if (Math.abs(result)<0.04f) result = 0;
		return result/SensorManager.GRAVITY_EARTH; //SensorManager.GRAVITY_EARTH;//(result>0.4f)?result/9.80665f:0;
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		TextView tvX= (TextView)findViewById(R.id.x_axis);
		TextView tvY= (TextView)findViewById(R.id.y_axis);
		TextView tvZ= (TextView)findViewById(R.id.z_axis);
		TextView tvG= (TextView)findViewById(R.id.gs);

		//ImageView iv = (ImageView)findViewById(R.id.image);
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		//float g;// = norm(x,y,z);
		if (!mInitialized) {
			mLastX = x;
			mLastY = y;
			mLastZ = z;
			dX = x; //0.0f;
			dY = y; //0.0f;
			dZ = z; //0.0f;
			dG = getG(x,y,z,wG); //0.0f;
			tvX.setText("0.0");
			tvY.setText("0.0");
			tvZ.setText("0.0");
			tvG.setText("0.0 G");
			mInitialized = true;
		} else {
			float deltaX = Math.abs(mLastX - x);
			float deltaY = Math.abs(mLastY - y);
			float deltaZ = Math.abs(mLastZ - z);
			//float deltaG = Math.abs(mLastG - g);
			dX = (deltaX < NOISE)?dX:x; //deltaX;
			dY = (deltaY < NOISE)?dY:y; //deltaY;
			dZ = (deltaZ < NOISE)?dZ:z; //deltaZ;
			if(!hardBrake) {
				dG = (dX==x //deltaX
					||dY==y//deltaY
					||dZ==z)? //deltaZ)?
					getG(dX,dY,dZ,wG)
				:
					dG;
			} else {
				dG = getG(dX,dY,dZ,wG);
				hardBrake = false;
			}
			//dG = (deltaG < NOISE/10.0f)?dG:deltaG;
			//float deltaG = Math.abs(mLastG - g);
			//if (deltaX < NOISE) deltaX = (float)0.0;
			//if (deltaY < NOISE) deltaY = (float)0.0;
			//if (deltaZ < NOISE) deltaZ = (float)0.0;
			//if (deltaG < NOISE) deltaG = mLastG;
			//if (deltaG < NOISE) deltaG = (float)0.0;
			mLastX = x;
			mLastY = y;
			mLastZ = z;
			tvX.setText(Float.toString(dX));
			tvY.setText(Float.toString(dY));
			tvZ.setText(Float.toString(dZ));
			tvG.setText(Float.toString(dG)+ " G");
			float hbFloat = (wG)?0.55f:-0.45f; 
			if (dG <= hbFloat) {
				hardBrake = true;
				TextView hb = (TextView) findViewById(R.id.hb);
				CharSequence ts = DateFormat.format("MM/dd/yy hh:mm:ss a", System.currentTimeMillis());
				hb.append("Hard braking detected at: " + ts +"\n");
			}
		}
	}
	private void preventScreenFromSleeping(boolean s) {
		View v;
		v = getWindow().getDecorView().findViewById(android.R.id.content);
		v.setKeepScreenOn(s);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch(buttonView.getId()) {
		case R.id.keep_screen_on:
			preventScreenFromSleeping(isChecked);
			break;
		case R.id.include_gravity:
			wG = isChecked;
			TextView tv = (TextView) findViewById(R.id.gs);
			String temp = (String)tv.getText();
			if (temp==null || temp=="") temp = "0.0";
			temp = temp.split(" ")[0];
																					Log.i("XXX", "value of temp = "+temp);
			float gravity = Float.valueOf(temp);
			if (wG) {
				gravity +=1;
			} else {
				gravity -=1;
			}
			dG = gravity;
			tv.setText(String.valueOf(gravity));
			break;
		}
	}
	
	public class SendData extends AsyncTask<String, Void, Void> {

		public SendData() {
			
		}

		@Override
		protected void onPreExecute() {
			// Runs on the UI thread before doInBackground(Params...).
		}

		@Override
		protected Void doInBackground(String... arguments) {
			// Override this method to perform a computation on a background thread.
			// The specified parameters are the parameters passed to execute(Params...)
			// by the caller of this task. This method can call publishProgress(Progress...)
			// to publish updates on the UI thread.
			return null;
		}


		@Override
		protected void onCancelled() {
			// Runs on the UI thread after cancel(boolean) is invoked and
			// doInBackground(Object[]) has finished. The default implementation simply
			// invokes onCancelled() and ignores the result. If you write your own
			// implementation, do not call super.onCancelled(result).
		}

	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()) {
		case R.id.clear:
			Log.i("XXX","HB Logs cleared.");
			TextView hb = (TextView) findViewById(R.id.hb);
			hb.setText("");
			break;
		case R.id.sens_add:
			if (NOISE>0.0f) {
				NOISE-=0.5f;
				updateSensitivityBar(NOISE);
			}
			break;	
		case R.id.sens_sub:
			if (NOISE<4.0f) {
				NOISE+=0.5f;
				updateSensitivityBar(NOISE);
			}
			break;
		}
		
	}
	private void updateSensitivityBar(float noise){
		String crude = "";
		if (noise==0.0f)		crude="--------*";
		else if (noise==0.5f)	crude="-------*-";
		else if (noise==1.0f)	crude="------*--";
		else if (noise==1.5f)	crude="-----*---";
		else if (noise==2.0f)	crude="----*----";
		else if (noise==2.5f)	crude="---*-----";
		else if (noise==3.0f)	crude="--*------";
		else if (noise==3.5f)	crude="-*-------";
		else if (noise==4.0f)	crude="*--------";
		TextView tv = (TextView) findViewById(R.id.sens);
		tv.setText(crude);
		
	}

	@Override
	public void onOrientationChange(float gyx, float gyy, float gyz) {
		// TODO Auto-generated method stub
		displayGyroReadings(gyx,gyy,gyz);
	}
	
	private void displayGyroReadings(float ggx, float ggy, float ggz) {
		TextView tx,ty,tz;
		tx = (TextView) findViewById(R.id.gx_axis);
		ty = (TextView) findViewById(R.id.gy_axis);
		tz = (TextView) findViewById(R.id.gz_axis);
		if (gyro.isPresent) {
			tx.setText(String.valueOf(ggx));
			ty.setText(String.valueOf(ggy));
			tz.setText(String.valueOf(ggz));
		} else {
			tx.setText("N/A");
			ty.setText("N/A");
			tz.setText("N/A");
		}
	}
}
