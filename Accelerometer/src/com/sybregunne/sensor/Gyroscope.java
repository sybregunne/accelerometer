package com.sybregunne.sensor;

import java.util.ArrayList;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Gyroscope implements SensorEventListener{
	/**
	 * Difference between current and previous values for device orientation;
	 */
	public float dX, dY, dZ;
	/**
	 * Previously stored orientation for the device
	 */
	public float prevX, prevY, prevZ;
	/**
	 * Current x, y, z orientation of the device
	 */
	public float x, y, z;
	/**
	 * true if Gyroscope is present in device
	 */
	public boolean isPresent = false;
	
	public float NOISE_TOLERANCE = 4.0f;
	public boolean USE_DEGREES = false;
	
	//Private variables
	private Context mAppContext = null;
	private SensorManager mSensorManager; 
	private Sensor mGyroscope;
	private boolean isInitialized=false;
	
	ArrayList<OnOrientationChangeListener> gD = new ArrayList <OnOrientationChangeListener>();
	/**
	 * 
	 * @param _context calling activity's or service's context
	 */
	public Gyroscope(Context _context) {
		mAppContext = _context.getApplicationContext();
		mSensorManager = (SensorManager) mAppContext.getSystemService(Context.SENSOR_SERVICE);
		mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		//if (null == mGyroscope) isPresent = false;
		register(); //isPresent = mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
		clear();
	}
	
	/**
	 * Resets all variables associated with this Gyroscope class.
	 */
	public void clear() {
		prevX=0.0f;
		prevY=0.0f;
		prevZ=0.0f;
		x=0.0f;
		y=0.0f;
		z=0.0f;
		dX=0.0f;
		dY=0.0f;
		dZ=0.0f;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO research this subject for greater accuracy in app. 
		
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		
		if(event.accuracy==SensorManager.SENSOR_STATUS_UNRELIABLE) {
			return;
		}
		x = event.values[0];
		y = event.values[1];
		z = event.values[2];
		if (!isInitialized) {
			prevX = x;
			prevY = y;
			prevZ = z;
			isInitialized = true;
		} else {
			dX = x - prevX;
			dY = y - prevY;
			dZ = z - prevZ;
			if (Math.abs(dX) > NOISE_TOLERANCE) {
				x = prevX;
			} else {
				prevX = x;
			}
			if (Math.abs(dY) > NOISE_TOLERANCE) {
				y = prevY;
			} else {
				prevY = y;
			}
			if (Math.abs(dZ) > NOISE_TOLERANCE) {
				z = prevZ;
			} else {
				prevZ = z;
			}
		}
		fireListeners(x,y,z);
	}
	private static float rad2Degs(float degrees) {
		// multiplied by 180 degs divided by value of pi
		return degrees * 180 / (float) Math.PI;
	}
	
	public static float degs2Rads(float radians) {
		//multiplied by value of PI divided by 180 degrees
		return radians * (float) Math.PI / 180;
	}
	
	public static interface OnOrientationChangeListener {
		public abstract void onOrientationChange(float x, float y, float z);
	}
	
	public void setOnOrientationChangedListener(OnOrientationChangeListener listener) {
		gD.add(listener);
	}
	public void register() {
		isPresent = mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
	}
	public void deRegister() {
		mSensorManager.unregisterListener(this);
	}
	private void fireListeners(float _x, float _y, float _z) {
		float tx,ty,tz;
		if (USE_DEGREES) {
			tx = rad2Degs(_x);
			ty = rad2Degs(_y);
			tz = rad2Degs(_z);
		} else {
			tx = _x;
			ty = _y;
			tz = _z;
		}
		for (OnOrientationChangeListener listener : gD) {
			listener.onOrientationChange(tx, ty, tz);
		}
	}
}
